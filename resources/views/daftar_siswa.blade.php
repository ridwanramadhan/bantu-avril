<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body>
    <!-- navbar -->
    @include('layouts.navbar')
    <div class="container">
        <h1>Siswa</h1>
        @foreach($siswa as $student)
        <ul>
            <li>{{$student}}</li>
        </ul>
        @endforeach
    </div>
    <!-- footer -->
    @include('layouts.footer')
    @include('layouts.js')
</body>
</html>