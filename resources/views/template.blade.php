<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body>
    
    
    <!-- main -->
    @yield('main')

    <!-- footer -->
    @include('layouts.footer')
    @include('layouts.js')
</body>
</html>