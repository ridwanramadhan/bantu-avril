<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('about', 'PagesController@homepage');
Route::get('rahasia', ['as' => 'secret', function () {
    return view('rahasia');
}]);
Route::get('show-secret', function () {
    return redirect()->route('secret');
});
Route::get('halaman1', function () {
    return view('halaman1');
});
Route::get('halaman2', function () {
    return view('halaman2');
});
Route::get('halaman3', function () {
    return view('halaman3');
});
Route::get('halaman4', function () {
    return view('halaman4');
});
Route::get('halaman5', function () {
    return view('halaman5');
});
Route::get('subhalaman1', function () {
    return view('page.halaman1');
});
Route::get('subhalaman2', function () {
    return view('page.halaman2');
});
Route::get('subhalaman3', function () {
    return view('page.halaman3');
});
Route::get('create', function () {
    return view('siswa.create');
});
Route::get('siswa', function () {
    $siswa = ['Muhammad Ridwan Ramadhan', 'Alvin Tandiardi', 'Mahdi Widianto'];
    return view('daftar_siswa', compact('siswa'));
});
